﻿CREATE TABLE [dbo].[LastVersion]
(
    [Key] NVARCHAR(32) NOT NULL PRIMARY KEY, 
    [Value] BIGINT NOT NULL
)
