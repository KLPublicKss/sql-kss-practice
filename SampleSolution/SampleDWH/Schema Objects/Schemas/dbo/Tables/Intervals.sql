﻿CREATE TABLE [dbo].[Intervals]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), 
	[RowVersion] ROWVERSION NOT NULL,
    [CustomerName] NVARCHAR(64) NOT NULL, 
    [ProductCode] NVARCHAR(32) NOT NULL,
    [ActivateDate] DATETIME NOT NULL, 
    [DeactivateDate] DATETIME NULL
)
