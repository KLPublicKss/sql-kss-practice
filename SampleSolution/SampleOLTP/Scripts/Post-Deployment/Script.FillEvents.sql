

INSERT INTO [dbo].[Events] ([CustomerName], [ProductCode], [CommandType], [CommandDate]) VALUES
	('Andrey', 'KIS', 'Activate', '2018-01-01 00:00'),
	('Masha', 'KIS', 'Activate', '2018-01-02 00:00'),
	('Vladimir', 'KAV', 'Activate', '2018-02-01 00:00'),
	('Vladimir', 'KAV', 'Deactivate', '2018-03-07 00:00'),
	('Andrey', 'KAV', 'Activate', '2018-02-01 00:00'),
	('Andrey', 'KIS', 'Deactivate', '2018-02-05 00:00');
