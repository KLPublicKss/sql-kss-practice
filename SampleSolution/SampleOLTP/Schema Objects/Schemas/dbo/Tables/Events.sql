﻿CREATE TABLE [dbo].[Events]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [CustomerName] NVARCHAR(64) NOT NULL, 
    [ProductCode] NVARCHAR(32) NULL,
    [CommandType] VARCHAR(16) NOT NULL, 
    [CommandDate] DATETIME NOT NULL,
)
